//
//  ViewController.swift
//  PhotoLibraryiOS
//
//  Created by Jiji🌸 on 5/10/21.
//

import UIKit
import Toast_Swift

class PhotosController: UIViewController {
    // The collection which will diaply the photos in cells.
    @IBOutlet weak var photosCollectionView: UICollectionView!
    // Manager controls the server photos' requets and responses.
    var manager : PhotosManager?
    // Manager controls selecting photos from gallery.
    var imagePickerManager: ImagePickerManager?
    // Server photos instance.
    var photos: [Photo] = []
    // Selected gallery images.
    var images: [UIImage] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Request the photos before the view appears to be sure
        // that all the components has been loaded.
        manager?.getPhotos()
    }
    // Action fires when the user clicks the button.
    @IBAction func PickImageAction(_ sender: UIButton) {
        // Check that the image picker is initialized and present it.
        guard let picker = imagePickerManager?.imagePicker else {return}
        self.present(picker, animated: true)
    }
}

//MARK:- Helper methods
extension PhotosController {
    // Add initial configurations of PhotosController and its associated components
    private func config() {
        manager = PhotosManager()
        manager?.delegate = self
        imagePickerManager = ImagePickerManager()
        imagePickerManager?.delegate = self
        self.photosCollectionView.dataSource = self
        self.photosCollectionView.delegate = self
        self.view.makeToastActivity(.center)
    }
}


//MARK:- UICollectionViewDelegate
// The delegate is important because it activate the UICollectionViewDelegateFlowLayout
extension PhotosController: UICollectionViewDelegate {}

//MARK:- UICollectionViewDataSource
extension PhotosController: UICollectionViewDataSource {
    // Set the number of section included in the collection view.
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    // Set the number of items in a section at the collection view.
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count + images.count
    }
    
    // Configure the collection view cell and pass its data.
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Get an instance of a reusable cell of type PhotoCell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCell.REUSE_IDENTIFIRE, for: indexPath) as! PhotoCell
        //Append the suitable data unit to cell.
        // Display the server phots first, then display the newely selected images before uploading them to the server to prevent waiting for their urls.
        if indexPath.row < photos.count {
            cell.bind(photo: photos[indexPath.row])
        }else {
            cell.bind(image: images[indexPath.row - photos.count])
        }
        return cell
    }
}

//MARK:- UICollectionViewDelegateFlowLayout
extension PhotosController: UICollectionViewDelegateFlowLayout {
    //Customize the collection view cell's size.
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // Size the photo cell to be almost 1/3 of the screen width
        let unit = (photosCollectionView.frame.size.width/3 ) - 10
        return CGSize(width: unit, height: unit)
    }
}

//MARK:- PhotosManagerDelegate
extension PhotosController: PhotosManagerDelegate {
    func requestFailed(error: String) {
        DispatchQueue.main.async {
            // Hide the activity indicator
            self.view.hideToastActivity()
            // Display the error message on top of the screen
            self.view.makeToast(error, duration: 3.0, position: .top)
        }
    }
    
    func didFetchPhotos(photos: [Photo]) {
        DispatchQueue.main.async {
            //Hide the activity indicator
            self.view.hideToastActivity()
            self.photos.append(contentsOf: photos)
            self.photosCollectionView.reloadData()
        }
    }
    
    func didUploadPhoto(photo: Photo) {
        DispatchQueue.main.async {}
    }
}

//MARK:- ImagePickerManagerDelegate
// Implement the actions after selecting images from gallery.
extension PhotosController: ImagePickerManagerDelegate {
    func didSelectImages(images: [UIImage]) {
        self.images.append(contentsOf: images)
        // Reload the data again
        photosCollectionView.reloadData()
        // After successfully getting the selected images ask the manager to
        // upload them on s3 then it will upload the photo url to
        // the server.
        // REFACTORING:- This logic needs to be refactored to ..
        // 1- ask the manger to upload any number of images.
        // 2- the manager implicitly upload each image to s3 and get a url for it, upload the url to our server, then delegate the new photo to this controller to work with it.
        for image in images {
            // Uploads a photo to S3 and implicitly upload it to our server and returns a photo object with an image url.
            manager?.uploadImageToS3(image: image)
        }
    }
}
