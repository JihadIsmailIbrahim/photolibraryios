//
//  Photo.swift
//  PhotoLibraryiOS
//
//  Created by ❤️🌸Jiji❤️ on 5/10/21.
//

import Foundation


// The main app's data unit
struct Photo: Codable {
    var url: String?
}
