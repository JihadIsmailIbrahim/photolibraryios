//
//  ResponseError.swift
//  PhotoLibraryiOS
//
//  Created by ❤️🌸Jiji❤️ on 5/11/21.
//

import Foundation


class ResponseError: Codable {
    var error: String?
}
