//
//  PhotosManager.swift
//  PhotoLibraryiOS
//
//  Created by ❤️🌸Jiji❤️ on 5/10/21.
//

import Foundation
import UIKit
import Alamofire

// Photos routes
struct PhotosUrls {
    static let GET_POST_PHOTOS = "http://localhost:3000/photos"
}

// PhotosManagerDelegate
protocol PhotosManagerDelegate: BaseManagerDelegate {
    func didFetchPhotos(photos: [Photo])
    func didUploadPhoto(photo: Photo)
}

//PhotosManagerProtocol
protocol PhotosManagerProtocol {
    var delegate: PhotosManagerDelegate? { get set }
    func getPhotos()
    func uploadImageToS3(image: UIImage)
    func uploadPhoto(url: String)
}

class PhotosManager: BaseManager, PhotosManagerProtocol {
    var delegate: PhotosManagerDelegate?
    
    override init() {
        super.init()
        AwsS3Manager.shared.delegate = self
    }
    
    func getPhotos() {
        request(url: PhotosUrls.GET_POST_PHOTOS).responseJSON { (response) in
            guard let error = response.error else {
                guard let data = response.data else {
                    self.delegate?.requestFailed(error: "Data not found")
                    return
                }
                do {
                    // try to parse the response's data as array of photos.
                    let photos = try JSONDecoder().decode([Photo].self, from: data)
                    // Send the fetched photos to the appropriate delegate.
                    self.delegate?.didFetchPhotos(photos: photos)
                }catch{
                    // Check if the response data was an error?!
                    do{
                        let serverError = try JSONDecoder().decode(ResponseError.self, from: data)
                        self.delegate?.requestFailed(error: serverError.error ?? "Undefined error!")
                    
                    }catch{
                        // If the parser failed to parse the response data, delgate its error to the user to know that there is an error.
                        self.delegate?.requestFailed(error: error.localizedDescription)
                    }
                }
                return
            }
            // If the request failed, delgate its error to the user to know that there is an error.
            self.delegate?.requestFailed(error: error.localizedDescription)
        }
    }
    
    //Convert the image to url by uploading it on S3
    // then upload it to our server.
    func uploadImageToS3(image: UIImage) {
        AwsS3Manager.shared.uploadPhoto(image: image)
    }
    
    // Upload the photo url to our server.
    func uploadPhoto(url: String) {
        //Prepare the body params for post request.
        var params = Parameters()
        params["url"] = url
        //Alamofire request.
        request(url: PhotosUrls.GET_POST_PHOTOS, method: .post, params: params).responseJSON { (resposne) in
            // Check for errors.
            guard let error = resposne.error else {
                guard let data = resposne.data else {
                    self.delegate?.requestFailed(error: "Data not found")
                    return
                }
                do {
                    // try to parse the response's data photo.
                    let photo = try JSONDecoder().decode(Photo.self, from: data)
                    // Send the uploaded photo to the appropriate delegate.
                    self.delegate?.didUploadPhoto(photo: photo)
                }catch{
                    // Check if the response data was an error?!
                    do{
                        // try to parse server error.
                        let serverError = try JSONDecoder().decode(ResponseError.self, from: data)
                        self.delegate?.requestFailed(error: serverError.error ?? "Undefined error!")
                    
                    }catch{
                        // If the parser failed to parse the response data, delgate its error to the user to know that there is an error.
                        self.delegate?.requestFailed(error: error.localizedDescription)
                    }
                }
                return
            }
            // If the request failed, delgate its error to the user to know that there is an error.
            self.delegate?.requestFailed(error: error.localizedDescription)

        }
    }
}

// MARK:- AwsS3ManagerDelegate
extension PhotosManager: AwsS3ManagerDelegate {
    func didFailToUpload(with error: String) {}
    func successToUpload(with url: String) {
        uploadPhoto(url: url)
    }
}
