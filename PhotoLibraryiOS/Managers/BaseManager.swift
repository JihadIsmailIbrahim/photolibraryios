//
//  BaseManager.swift
//  PhotoLibraryiOS
//
//  Created by ❤️🌸Jiji❤️ on 5/10/21.
//

import Foundation
import Alamofire

// Common response delegates.
protocol BaseManagerDelegate {
    func requestFailed(error: String)
}

// Set the base manager that manges requests, and add the common methods in it.
class BaseManager {
    //Alamofire Basic request.
    func request(url: String, method: HTTPMethod = .get, params: Parameters? = nil)-> DataRequest {
        return AF.request(url, method: method, parameters: params, encoding: JSONEncoding.default)
    }
    
}
