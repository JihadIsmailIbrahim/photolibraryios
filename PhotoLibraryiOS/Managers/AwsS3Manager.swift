//
//  AwsS3Manager.swift
//  PhotoLibraryiOS
//
//  Created by ❤️🌸Jiji❤️ on 5/11/21.
//

import Foundation
import UIKit
import AWSS3
import AWSCore

// This is the credentials of s3, but its a bad behavior to place it at the application files, it must be stored in a secured file that is not pushed on git also.
struct AwsS3Credentilas {
    static let ACCESS_KEY = "AKIASVQSPDCSBWZD6MFN"
    static let SECRET_KEY = "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"
    static let BUCKET_NAME = "photosdisply"
}

// Delegate transfers AWSS3 responses to its listerners.
protocol AwsS3ManagerDelegate {
    func didFailToUpload(with error: String)
    func successToUpload(with url: String)
}

protocol AwsS3ManagerProtocol {
    var delegate: AwsS3ManagerDelegate? {set get}
    func uploadPhoto(image: UIImage)
    
}
// Singlton manager controls uploading the selected photos to s3
class AwsS3Manager: AwsS3ManagerProtocol {
    static let shared = AwsS3Manager()
    var delegate: AwsS3ManagerDelegate?
    
    // Add initial configurations
    private init() {
        let provider = AWSStaticCredentialsProvider(accessKey: AwsS3Credentilas.ACCESS_KEY, secretKey: AwsS3Credentilas.SECRET_KEY)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.EUWest2, credentialsProvider: provider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
    }
    
    // Write an image in memory with a path.
    private func createImageUriAndName(image: UIImage, completionHandler: (_ imageUri: URL, _ imageName: String) -> Void) {
        guard let imageData = image.jpegData(compressionQuality: 1.0) else {
            return
        }
                
        let tempPath = NSTemporaryDirectory() as String
        let fileName: String = ProcessInfo.processInfo.globallyUniqueString + (".jpeg")
        let filePath = tempPath + "/" + fileName
        let fileUri = URL(fileURLWithPath: filePath)
        do {
            try imageData.write(to: fileUri)
            completionHandler(fileUri,fileName)
        } catch {
            return
        }
    }
    
    //The request to upload an image to s3 using its path(imageUri) and its name.
    private func uploadPhoto(imageUri: URL, imageName: String) {
        // The completion block wich should be fired when the upload task complets.
        var completionHandler : AWSS3TransferUtilityUploadCompletionHandlerBlock?

        //Implementation for the task completion resposne.
        completionHandler = { (task, error) in
            // Sinec the task is performed in another thread, you must switch to work on the main thread.
            DispatchQueue.main.async {
                // Check for errors
                guard let err = error else {
                    //Construct the image url after success upload.
                    // AWSS3 base url + AWSS3 bucket name + image name
                    let awsBaseUrl = AWSS3.default().configuration.endpoint.url
                    let publicURL = awsBaseUrl!.appendingPathComponent(AwsS3Credentilas.BUCKET_NAME).appendingPathComponent(imageName)
                    // Send the success url to the listener component.
                    self.delegate?.successToUpload(with: publicURL.absoluteString)
                    return
                }
                // Send the failure error to the listener component.
                self.delegate?.didFailToUpload(with: err.localizedDescription)
            }
        }
        
        
        // S3 upload task.
        let awsTransferUtitlity = AWSS3TransferUtility.default()
        awsTransferUtitlity.uploadFile(imageUri, bucket: AwsS3Credentilas.BUCKET_NAME, key: imageName, contentType: "image", expression: nil, completionHandler: completionHandler).continueWith { (task) in
            return nil
        }
    }
}


extension AwsS3Manager {
    // Write the image in memory with a path and name, then upload the image to s3 by specifing that path and image name.
    func uploadPhoto(image: UIImage) {
        // Create the comletion handler after creating a uri for the image.
        let completionHandler =  { (imageUri, imageName) in
            self.uploadPhoto(imageUri: imageUri, imageName: imageName)
        }
        
        createImageUriAndName(image: image, completionHandler: completionHandler)
    }
}
