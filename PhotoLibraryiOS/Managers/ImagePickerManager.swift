//
//  ImagePickerManager.swift
//  PhotoLibraryiOS
//
//  Created by ❤️🌸Jiji❤️ on 5/11/21.
//

import Foundation
import DKImagePickerController


// Delegate which transfer the ImagePickerManager data to
// delegate controllers
protocol ImagePickerManagerDelegate {
    func didSelectImages(images: [UIImage])
}

// Manager controls the requests to select images from user gallery.
class ImagePickerManager {
    var delegate: ImagePickerManagerDelegate?
    var imagePicker: DKImagePickerController?
     
    init() {
        config()
    }
    
    func config(){
        imagePicker = DKImagePickerController()
        // Determin the maximum number of photos to be selected at a time.
        imagePicker?.maxSelectableCount = 10
        // Allow selecting by swiping your hand on photos.
        imagePicker?.allowSwipeToSelect = true
        // Determin the media type which will select from.
        imagePicker?.sourceType = .photo
        imagePicker?.assetType = .allPhotos
        // Only allow selecting Photos.
        imagePicker?.allowMultipleTypes = false
        // Display the cancel button.
        imagePicker?.showsCancelButton = true
        
        // Cancel handler
        // Deselect all the selected images when canceling.
        imagePicker?.didCancel = {
            self.imagePicker?.deselectAll()
        }
        
        // convert the assets to images and transfer them to the
        // controller to display them.
        //Deselect the phtos after getting the selected images from their assets.
        imagePicker?.didSelectAssets = {
            (assets: [DKAsset]) in
            var images: [UIImage] = []
            for asset in assets {
                asset.fetchImage(with: CGSize(width: 200, height: 200)) { image, info in
                    if let img = image {
                        images.append(img)
                    }
                }
            }
            self.delegate?.didSelectImages(images: images)
            images.removeAll()
            self.imagePicker?.deselectAll()
        }
    }
}

