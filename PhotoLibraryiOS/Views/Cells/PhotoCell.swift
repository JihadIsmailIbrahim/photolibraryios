//
//  PhotoCell.swift
//  PhotoLibraryiOS
//
//  Created by ❤️🌸Jiji❤️ on 5/10/21.
//

import UIKit
import SDWebImage

// The class which will render the url and convert it to an image
class PhotoCell: UICollectionViewCell {
    static let REUSE_IDENTIFIRE = "PhotoCell"
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        // Remove the associated image when prearing for reuse to not reuse the same photo in another cell!
        photoImageView.image = nil
    }
    
    // Display the server photo on the image view. by downloading its url using SDWebImage
    func bind(photo: Photo) {
        guard let urlString = photo.url, let url = URL(string: urlString) else {
            photoImageView.image = nil
            return
        }
        
        photoImageView.sd_setImage(with: url, placeholderImage: nil, options: SDWebImageOptions.scaleDownLargeImages, context: nil)
    }
    
    // Add the selected gallery image to the imageView component.
    func bind(image: UIImage){
        photoImageView.image = image
    }
}
