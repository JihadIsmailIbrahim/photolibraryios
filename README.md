**To use this project**
	**** YOU MUST RUN THE PhotoLibraryAPI project first ****

1. Clone the project to your machine.

2. Open your terminal

	* **cd** to the project directory.
	* Run **pod insatll**.
	
3. Go to the xcode and open the project from it.

4. Run the project and **make sure to run the API project first**.